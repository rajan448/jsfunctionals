const double = x => x*2;
const triple = x => x*3;
const quadruple = x => x*4;

/*

    const pipe = (...functions) => input => functions.reduce(
        function(result, currentFunction){
            return currentFunction(result);
    }, input);

*/

const pipe = (...functions) => input => functions.reduce(
    (result, currentFunction) => currentFunction(result), input
);

const multiplySix = pipe(double, triple);
const multiply24x = pipe(double, triple, quadruple);

console.log(multiplySix(10));
console.log(multiply24x(10));
