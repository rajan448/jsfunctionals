
const arr = [1,2,3,4,5,6,2,3,4,1,3,4,5,6,3];

let result = arr.sort().reduce(function (accumulator, currentVal) {
    let len = accumulator.length;
    if(len === 0 || accumulator[len-1] !== currentVal) {
        accumulator.push(currentVal);
    }
    return accumulator;
}, []);

console.log(result);
