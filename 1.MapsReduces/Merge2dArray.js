
arr = [
    [1,2,3],
    [4,5],
    [6,7,8,9]
];

console.log(arr);
console.log(merge2dArray(arr));

console.log(merge2dFunctional(arr));

// The Imperative way
function merge2dArray(arrays) {
    let count = arrays.length;
    let merged = new Array(count);

    let c = 0;

    for(let i=0; i<count; i++) {
        for(let j=0, jLen = arrays[i].length; j< jLen; j++) {
            merged[c] = arrays[i][j];
            c++;
        }
    }
    return merged;
}


// Functional Way
function merge2dFunctional(arrays) {
    return arrays.reduce(function (accumulator, currentVal) {
        return accumulator.concat(currentVal);
    }, []);
}
